#pragma once

class Katze
{
public:
	Katze::Katze();
	Katze::Katze(float value);

	void makeSound();

private:
	float m_value;

};